Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libei
Source: https://gitlab.freedesktop.org/libinput/libei

Files: *
Copyright: 2020-2023 Red Hat
License: Expat

Files: debian/*
Copyright: 2023 Canonical Ltd
License: Expat

Files: munit/*
Copyright: 2013-2018 Evan Nemerson
License: Expat

Files: doc/api/doxygen-awesome.css
Copyright: 2021 jothepro
License: Expat
Comment: Doxygen Awesome from https://github.com/jothepro/doxygen-awesome-css

Files: proto/protocol.xml
Copyright: 2008-2011 Kristian Høgsberg
           2010-2011 Intel Corporation
           2012-2013 Collabora, Ltd.
           2023 Red Hat, Inc.
License: Expat

Files: src/util-strings.*
Copyright: 2008 Kristian Høgsberg
           2013-2015 Red Hat, Inc.
License: Expat

Files: src/util-bits.h
       src/util-list.*
       src/util-macros.h
Copyright: 2008-2011 Kristian Høgsberg
           2011 Intel Corporation
           2013-2015 Red Hat, Inc.
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice (including the next
 paragraph) shall be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.
