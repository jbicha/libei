
/* SPDX-License-Identifier: MIT */
/*
 * Copyright © 2020 Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include "util-object.h"
#include "brei-shared.h"

struct eis;
struct eis_client;

/* This is a protocol-only object, not exposed in the API */
struct eis_connection {
	struct object object;
	struct brei_object proto_object;

	struct list pending_pingpongs;
};

OBJECT_DECLARE_GETTER(eis_connection, context, struct eis *);
OBJECT_DECLARE_GETTER(eis_connection, id, object_id_t);
OBJECT_DECLARE_GETTER(eis_connection, version, uint32_t);
OBJECT_DECLARE_GETTER(eis_connection, client, struct eis_client *);
OBJECT_DECLARE_GETTER(eis_connection, proto_object, const struct brei_object *);
OBJECT_DECLARE_GETTER(eis_connection, interface, const struct eis_connection_interface *);
OBJECT_DECLARE_REF(eis_connection);
OBJECT_DECLARE_UNREF(eis_connection);

struct eis_connection *
eis_connection_new(struct eis_client *client);


/**
 * Called when the ei_callback.done request is received after
 * an ei_connection_ping() event.
 */
typedef void (*eis_connection_ping_callback_t)(struct eis_connection *connection,
					       void *user_data);

void
eis_connection_ping(struct eis_connection *connection,
		    eis_connection_ping_callback_t callback,
		    void *user_data);
